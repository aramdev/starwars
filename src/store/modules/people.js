import server from '@/server'

export default {
  namespaced: true,
  state: {
    count: null,
  },
  getters: {
    count: s => s.count,
  },
  mutations: {
    setCount(state, data){
      state.count = data
    },
  },
  actions: {
    async fetchMoreInfo(store, link){
      try {
        const res = await fetch(link) 
        const data = await res.json()
        return data
      }
        catch (e){
        console.log(e)
        throw e   
    }
    },
    async load(store, pageId){
      try {
        const res = await fetch(`https://swapi.dev/api/people/?page=${pageId}`)
        
        if(res.status === 200){
            
          const data = await res.json()
          console.log(data)
          const results = data.results
          if(!store.state.count){
            store.commit('setCount', data.count)
          }

          let people = await Promise.all(results.map(async (r) => {
            let person = {
              name: r.name,
              gender: r.gender,
              height: r.height,
              mass: r.mass,
            }
            let starships = await r.starships.map((url) => store.dispatch('fetchMoreInfo', url))
            await Promise.all(starships).then((data) => {
              person.starships = data.map(el => el.name)
            })
            
            return person
          }))

          return people
        }else{
          return []
        }
      } catch(e){
        console.log(e)
        throw e
      }
       
    },
    async fetchPerson(store, id){
      try {
        const res = await fetch(`https://swapi.dev/api/people/${id}/`, {
          header: 'Access-Control-Allow-Origin',
        })
        if(res.status === 200){
          const data = await res.json()
          
          let person = {
            name: data.name,
            birth_year: data.birth_year,
            gender: data.gender,
            height: data.height,
            mass: data.mass,
            hair_color: data.hair_color,
            skin_color: data.skin_color,
            eye_color: data.eye_color,
          }

          let films = await data.films.map((url) => store.dispatch('fetchMoreInfo', url))
          await Promise.all(films).then((data) => {
            person.films = data.map(el => el.title)
          })


          return person
        }else{
          return null
        }
      } catch(e){
        console.log(e)
        throw e
      }
    }
  }
}
