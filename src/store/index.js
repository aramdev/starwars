import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import people from './modules/people';



export default new Vuex.Store({
  modules: {
    people,
  },
  strict: process.env.NODE_ENV !== 'production'
})
