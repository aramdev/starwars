import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'



Vue.use(VueRouter)


const routes = [
  {
    name: 'home',
    path: '',
    redirect: '/people'
  },
  {
    path: '/people',
    name: 'people',
    component: () => import('@/views/People.vue'),
  },
  {
    path: '/person/:id',
    name: 'person',
    component: () => import('@/views/Person.vue'),
  },
  {
    path: '*',
    component: () => import('@/views/E404.vue')
  }
  
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
