import axios from 'axios';

let server = axios.create({
    baseURL: '/api',
    withCredentials: true
});

export default server;