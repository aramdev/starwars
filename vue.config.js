let isProd = process.env.NODE_ENV === 'production';
let path = require('path')

let config = {
	publicPath: isProd ? '' : '/',
	productionSourceMap: false,
	configureWebpack: (config) => {
		return {
            devServer: {
                proxy: {
                    '/api': {
                        target: 'https://swapi.dev',
                        secure: false,
                        changeOrigin: true
                    }
                }
                
            }
        }
	}
};



module.exports = config;